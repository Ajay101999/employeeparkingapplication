package com.hcl.empparking.service.impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.hcl.empparking.dtos.AllotmentsDto;
import com.hcl.empparking.dtos.ApiResponse;
import com.hcl.empparking.entity.Allotment;
import com.hcl.empparking.entity.ParkingSpots;
import com.hcl.empparking.enums.AllotmentStatus;
import com.hcl.empparking.enums.ParkingStatus;
import com.hcl.empparking.exception.EmployeeAlreadyHaveParkingSpotException;
import com.hcl.empparking.exception.EmployeeAlreadyRequestedException;
import com.hcl.empparking.exception.EmployeeNotFoundException;
import com.hcl.empparking.mapper.CustomMapper;
import com.hcl.empparking.repository.AllotmentsRepo;
import com.hcl.empparking.repository.EmployeeRepo;
import com.hcl.empparking.repository.ParkingSpotsRepo;
import com.hcl.empparking.service.EmployeeService;
import com.hcl.empparking.utils.Responses;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepo employeeRepo;
	private final ParkingSpotsRepo parkingSpotsRepo;
	private final AllotmentsRepo allotmentsRepo;

	@Override
	public ApiResponse applySpot(AllotmentsDto allotmentsDto) {
		if (!(employeeRepo.existsByEmpId(allotmentsDto.getEmpId()))) {
			log.info("Employee not found exception occured");
			throw new EmployeeNotFoundException(Responses.EMP_NOT_FOUND_MESSAGE);
		}
		Optional<ParkingSpots> emp = parkingSpotsRepo.findByEmpId(allotmentsDto.getEmpId());
		String parkingStatus = emp.get().getParkingStatus();

		if (parkingStatus.equals(ParkingStatus.Not_Available.name())) {
			log.info("Employee already have the Parking Spot");
			throw new EmployeeAlreadyHaveParkingSpotException(Responses.EMP_ALREADY_HAVE_PARKING_MSG);
		}
		
		Optional<Allotment> byEmpId = allotmentsRepo.findByEmpId(allotmentsDto.getEmpId());
		if(byEmpId.isPresent()) {
			if (byEmpId.get().getEmpId().equals(allotmentsDto.getEmpId())
					&& byEmpId.get().getStatus().equals(AllotmentStatus.Pending.name())) {
				log.info("Request already sent and is in pending status");
				throw new EmployeeAlreadyRequestedException(Responses.EMP_ALREADY_REQUESTED_MSG);
			}
		}
		Allotment newRequest = CustomMapper.dtoToAllotments(allotmentsDto, new Allotment());
		try {
			allotmentsRepo.save(newRequest);
			log.info("Emploee parking request data saved in database");
		} catch (Exception e) {
			log.info("Emploee parking request data not saved in database");
			log.info(e.getMessage());
			return new ApiResponse(Responses.EMP_DATA_NOT_INSERTED, Responses.EMP_DATA_NOT_INSERTED_MSG);
		}

		return new ApiResponse(Responses.EMP_PARKING_REQUEST_INSERTED, Responses.EMP_PARKING_REQUEST_INSERTED_MSG);
	}
}
