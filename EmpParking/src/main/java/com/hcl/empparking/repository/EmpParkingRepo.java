package com.hcl.empparking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.empparking.entity.EmpParking;

@Repository
public interface EmpParkingRepo extends JpaRepository<EmpParking, Long> {

}
