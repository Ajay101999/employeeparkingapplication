package com.hcl.empparking.dtos;

import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class AllotmentsDto {

	@Min(00001)
	private Long empId;

}
